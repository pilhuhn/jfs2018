
if [ ! -e target/frontend-thorntail.jar ]
then
	mvn clean install -Pthorntail
fi

java -jar target/frontend-thorntail.jar
